# LOUPER

## About

LOUPER application for live loop music performance.

For Louisa; Valentine's day 2022.

---

## Components

There are 4 main components in this project:

- `louper0` : library implementation of the loop recorder
- `louper`  : CLI front-end
- `louper-gui` : GUI front-end
- `louper-footpedal` : Hardware foot pedal remote control


## Dependencies

For build:

- cmake
- gcc or llvm
- doxygen for API documentation

For `louper0`:

- asound
- liblo
- libsigcpp
- libsndfile
- rtaudio (rtaudio[alsa])

For `louper`:

- argparse

For `louper-footpedal`:

- liblo
- wiringPi

For `louper-gui`:

- argparse
- imgui (imgui[opengl3-binding], imgui[sdl2-binding])
- gl3w
- glbinding
- sdl2 (sdl2[x11])

For unit tests:

- catch2

I can recommend using [vcpkg](https://github.com/microsoft/vcpkg) for dependency package management.


# License

This software is licensed under GPL 3.0; See LICENSE file.

```
// SPDX-License-Identifier: GPL-3.0-only
// Copyright © 2022 Doug Hammond
```
