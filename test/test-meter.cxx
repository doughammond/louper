/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <memory>
#include <vector>

#include <catch.hpp>

#include "impl/meter.h"

TEST_CASE("Meter with empty input")
{
    auto m = std::make_shared<Meter>(10, 10);

    const std::vector<float> b{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    m->update(b);

    REQUIRE(m->levels.peak == 0);
    REQUIRE(m->levels.peakdB == -30);
    REQUIRE(m->levels.RMS == 0);
    REQUIRE(m->levels.RMSdB == -30);
}

TEST_CASE("Meter with DC input")
{
    auto m = std::make_shared<Meter>(10, 10);

    const std::vector<float> b{0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f};
    m->update(b);

    REQUIRE(m->levels.peak == 0.5f);
    REQUIRE(m->levels.peakdB == -3.0103f);
    REQUIRE(m->levels.RMS == 0.5f);
    REQUIRE(m->levels.RMSdB == -3.0103f);
}

TEST_CASE("Meter with sine input")
{
    auto m = std::make_shared<Meter>(20, 20);

    const std::vector<float> b{
        0.0, 0.30901094055255124, 0.5877749528776627, 0.8090057698350391, 0.9510486479704771, 0.9999999994935276,
        0.9510683176261254, 0.8090431838207742, 0.5878264489990678, 0.30907147821305286, 6.365358975045955e-05,
        -0.30895040164000565, -0.5877234543747231, -0.8089683525713912, -0.9510289744613893, -0.9999999954417481,
        -0.9510879834282546, -0.8090805945284449, -0.5878779427387298, -0.3091320146212643};
    m->update(b);

    REQUIRE(m->levels.peak == Catch::Approx(0.93f));       // not 1.0 because of peakDecay
    REQUIRE(m->levels.peakdB == Catch::Approx(-0.31517f)); // not 0.0 because of peakDecay
    REQUIRE(m->levels.RMS == Catch::Approx(0.70711f));
    REQUIRE(m->levels.RMSdB == Catch::Approx(-1.50511f));
}