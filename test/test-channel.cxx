/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <memory>

#include <catch.hpp>

#include "impl/channel.h"

TEST_CASE("LoopRecorder cue state changes when empty")
{
    const auto c = std::make_shared<LoopRecorder>(0);
    REQUIRE(c);

    STATE cue = STATE::CLEAR;
    c->cue_change().connect(
        [&cue](const uint &, const STATE &c)
        {
            cue = c;
        });

    c->nextCueState(STATE::CLEAR);
    REQUIRE(cue == STATE::CLEAR);

    c->nextCueState(STATE::REPLACE);
    REQUIRE(cue == STATE::REPLACE);

    c->nextCueState(STATE::REPLACE);
    REQUIRE(cue == STATE::CLEAR);

    c->nextCueState(STATE::OVERDUB);
    REQUIRE(cue == STATE::REPLACE); // when empty

    c->nextCueState(STATE::OVERDUB);
    REQUIRE(cue == STATE::CLEAR); // when empty

    c->nextCueState(STATE::CLEAR);
    REQUIRE(cue == STATE::CLEAR);

    c->nextCueState(STATE::REPLACE);
    REQUIRE(cue == STATE::REPLACE);

    c->nextCueState(STATE::OVERDUB);
    REQUIRE(cue == STATE::CLEAR); // when empty

    c->nextCueState(STATE::REPLACE);
    REQUIRE(cue == STATE::REPLACE);

    c->nextCueState(STATE::CLEAR);
    REQUIRE(cue == STATE::CLEAR);

    c->nextCueState(STATE::OVERDUB);
    REQUIRE(cue == STATE::REPLACE); // when empty

    c->nextCueState(STATE::CLEAR);
    REQUIRE(cue == STATE::CLEAR);
}

TEST_CASE("LoopRecorder CLEAR->REPLACE->PLAY")
{
    const auto c = std::make_shared<LoopRecorder>(0);
    REQUIRE(c);

    STATE state;
    c->state_change().connect(
        [&state](const uint &, const STATE &s)
        {
            state = s;
        });

    // Enter REPLACE mode
    c->nextCueState(STATE::REPLACE);
    c->nextState();
    REQUIRE(state == STATE::REPLACE);

    // First REPLACE period
    Buffer in{0, 1, 2, 3};
    auto out = c->process(in);
    REQUIRE(out == Buffer{0, 0, 0, 0});

    // Second REPLACE period
    in = Buffer{4, 5, 6, 7};
    out = c->process(in);
    REQUIRE(out == Buffer{0, 0, 0, 0});

    // Exit REPLACE mode
    c->nextCueState(STATE::REPLACE);
    c->nextState();
    REQUIRE(state == STATE::PLAY);

    // Synced period length
    in = Buffer{0, 0, 0, 0};
    // First PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{0, 1, 2, 3});
    // Second PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{4, 5, 6, 7});

    // Un-synced period length
    in = Buffer{0, 0, 0};
    // First PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{0, 1, 2});
    // Second PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{3, 4, 5});
    // Third PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{6, 7, 0});
    // Fourth PLAY period
    out = c->process(in);
    REQUIRE(out == Buffer{1, 2, 3});
}
