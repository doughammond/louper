/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <memory>

#include <catch.hpp>

#include "impl/transport.h"

TEST_CASE("Transport bar frames calculation")
{
    const auto t = std::make_shared<Transport>();
    REQUIRE(t);

    uint32_t bar_frames = 0;
    t->bar_length().connect(
        [&bar_frames](uint32_t len)
        {
            bar_frames = len;
        });

    t->samplerate(48000);
    t->on_bpm(120.0);
    t->on_timesig(4, 4);
    REQUIRE(bar_frames == 2 * 96000); // 2x channels

    t->on_bpm(90.0);
    t->on_timesig(4, 4);
    REQUIRE(bar_frames == 2 * 128000); // 2x channels

    t->on_bpm(60.0);
    t->on_timesig(4, 4);
    REQUIRE(bar_frames == 2 * 192000); // 2x channels

    t->on_bpm(120.0);
    t->on_timesig(3, 4);
    REQUIRE(bar_frames == 2 * 72000); // 2x channels

    t->on_bpm(90.0);
    t->on_timesig(3, 4);
    REQUIRE(bar_frames == 2 * 96000); // 2x channels

    t->on_bpm(60.0);
    t->on_timesig(3, 4);
    REQUIRE(bar_frames == 2 * 144000); // 2x channels
}

TEST_CASE("Transport period status 1")
{
    const auto t = std::make_shared<Transport>();
    REQUIRE(t);

    uint32_t bar_frames = 0;
    t->bar_length().connect(
        [&bar_frames](uint32_t len)
        {
            bar_frames = len;
        });

    // 1/1000 typical values for easy testing
    t->samplerate(40);
    t->on_bpm(120.0);
    t->on_timesig(4, 4);
    REQUIRE(bar_frames == 160); // 2* channels

    // Test updating using frame length which
    // does not divide the bar equally
    auto ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{6});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{5});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{4});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{3});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{3});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{2});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{1});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 3);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 3);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{7});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{6});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{6});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 1);

    ts = t->update(7, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 1);
}

TEST_CASE("Transport period status 2")
{
    const auto t = std::make_shared<Transport>();
    REQUIRE(t);

    uint32_t bar_frames = 0;
    t->bar_length().connect(
        [&bar_frames](uint32_t len)
        {
            bar_frames = len;
        });

    t->samplerate(48000);
    t->on_bpm(120.0);
    t->on_timesig(4, 4);
    REQUIRE(bar_frames == 192000); // 2x channels

    // Test updating using frame length which
    // does not divide the bar equally
    auto ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{7616});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{7040});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{6464});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 1);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{5888});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{5888});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{5312});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{4736});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{4160});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 2);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{3584});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{3584});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{3008});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 2);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{2432});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 3);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{1856});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 3);
    REQUIRE(ts.beat == 4);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{1280});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{1280});
    REQUIRE(ts.bar == 4);
    REQUIRE(ts.beat == 1);

    ts = t->update(8192, 0);
    REQUIRE(ts.bar_frames == std::unordered_set<uint>{});
    REQUIRE(ts.beat_frames == std::unordered_set<uint>{});
    REQUIRE(ts.bar == 4);
    REQUIRE(ts.beat == 1);
}
