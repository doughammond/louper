/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <filesystem>
#include <iostream>
#include <sstream>

#include <SDL2/SDL.h>
#include <argparse/argparse.hpp>

#include "config.h"
#include "looper.h"

int main(int argc, char *argv[])
{
  std::cerr << LOOPER_NAME
            << " DSP version " << LOOPER_VER << " " << LOOPER_VER_DATE
            << std::endl
            << LOOPER_LICENSE
            << std::endl
            << LOOPER_COPYRIGHT
            << std::endl
            << std::endl;

  argparse::ArgumentParser program(LOOPER_NAME);

  program.add_argument("-l", "--loops")
      .default_value(3)
      .action([](const std::string &value)
              { return std::stoi(value); })
      .help("number of loop recorders");

  program.add_argument("-r", "--sample-rate")
      .default_value(44100)
      .required()
      .action([](const std::string &value)
              { return std::stoi(value); })
      .help("synthesis sample rate");

  program.add_argument("-b", "--buffer-size")
      .default_value(256)
      .required()
      .action([](const std::string &value)
              { return std::stoi(value); })
      .help("audio buffer size");

  try
  {
    program.parse_args(argc, argv);
  }
  catch (const std::runtime_error &err)
  {
    std::cout << err.what() << std::endl;
    std::cout << program;
    exit(-1);
  }

  const auto loops = program.get<int>("-l");
  const auto Fs = program.get<int>("-r");
  const auto bufsize = program.get<int>("-b");

  const auto looper = std::make_shared<Looper>(loops, Fs, bufsize);
  looper->start();

  while (true)
  {
    sleep(1);
  }

  looper->stop();

  return 0;
}
