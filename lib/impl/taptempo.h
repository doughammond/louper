/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <memory>
#include <sigc++/sigc++.h>

using SignalTempo = sigc::signal<void(const float &)>;

/**
 * @brief Tap tempo calculator
 */
class TapTempo
{
public:
    /**
     * @brief Shared pointer type for TapTempo
     *
     */
    using sptr = std::shared_ptr<TapTempo>;

    explicit TapTempo();
    ~TapTempo();

    /**
     * @brief Slot: receive tap event
     *
     */
    void on_tap();

    /**
     * @brief Getter for tempo signal
     *
     * @return SignalTempo&
     */
    SignalTempo &tempo()
    {
        return s_tempo;
    }

private:
    /**
     * @brief Signal: calculated tempo
     *
     */
    SignalTempo s_tempo;

    /**
     * @brief Absolute timestamp when current counting period started
     *
     */
    int64_t m_tap_start;

    /**
     * @brief Count of taps received in the current counting interval
     *
     */
    uint m_tap_count;
};
