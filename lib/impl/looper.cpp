/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <chrono>
#include <iostream>
#include <tgmath.h>
#include <thread>

#include "looper.h"

Looper::Looper(const size_t &channelCount, const uint32_t &samplerate, const uint &bufSize)
    : m_bufSize(bufSize)
{
    m_driver = std::make_shared<AudioDrv>(samplerate);
    m_transport = std::make_shared<Transport>();
    m_transport->tempo().connect(s_tempo);
    m_transport->timesig().connect(s_timesig);
    m_transport->bar_length().connect(s_bar_length);

    std::vector<IChannel::sptr> inputs;

    // Loops
    for (size_t i = 0; i < channelCount; ++i)
    {
        m_loop_channels.push_back(std::move(std::make_shared<LoopRecorder>(i)));
        m_loop_channels[i]->cue_change().connect(s_channel_cue);
        m_loop_channels[i]->state_change().connect(s_channel_state);
        m_loop_channels[i]->state_change().connect(
            [this](const uint &, const STATE &state)
            {
                // any channel play stops the metronome
                if (state == STATE::PLAY)
                {
                    m_mixer->cue_mute(m_loop_channels.size() + 1, true);
                }
            });
        m_loop_channels[i]->length().connect(s_channel_length);
        m_loop_channels[i]->position().connect(s_channel_position);

        m_transport->bar().connect(sigc::mem_fun(*m_loop_channels[i], &LoopRecorder::on_bar));

        inputs.push_back(m_loop_channels[i]);
    }

    // Monitor
    inputs.push_back(std::move(std::make_shared<Passthrough>()));
    // Metronome
    auto metronome = std::make_shared<Metronome>(m_driver->samplerate());
    m_transport->beat().connect(sigc::mem_fun(*metronome, &Metronome::on_beat));
    inputs.push_back(std::move(metronome));

    m_mixer = std::make_shared<Mixer>(inputs, m_driver->samplerate());
    m_mixer->levels().connect(s_levels);
    m_transport->bar().connect(sigc::mem_fun(*m_mixer, &Mixer::on_bar));

    m_driver->buffer().connect(sigc::mem_fun(*this, &Looper::on_buffer));

    m_control = std::make_shared<Control>(channelCount);
    m_control->channel_cue().connect(
        [this](const uint &channel, const STATE &cuestate)
        {
            std::cout << " looper channel " << channel << " cue " << cuestate << std::endl;
            m_loop_channels[channel]->nextCueState(cuestate);
        });
    m_control->channel_mute().connect(sigc::mem_fun(*m_mixer, &Mixer::cue_mute));
    m_control->channel_solo().connect(sigc::mem_fun(*m_mixer, &Mixer::cue_solo));
    m_control->control_cue().connect(s_control_cue);

    m_taptempo = std::make_shared<TapTempo>();
    m_control->taptempo().connect(sigc::mem_fun(*m_taptempo, &TapTempo::on_tap));
    m_taptempo->tempo().connect(sigc::mem_fun(*m_transport, &Transport::on_bpm));
    // [0..n-1] = loops
    // n = monitor
    // n+1 = metronome
    m_mixer->cue_mute(m_loop_channels.size() + 1, true); // start metronome muted
    m_control->taptempo().connect(
        [this]()
        {
            // Pressing tap unmutes the metronome
            m_mixer->cue_mute(m_loop_channels.size() + 1, false);
        });
}

Looper::~Looper()
{
}

void Looper::start()
{
    std::cout << "Looper start" << std::endl;

    m_transport->reset();
    m_transport->samplerate(m_driver->samplerate());
    m_transport->on_bpm(120.0);
    m_transport->on_timesig(4, 4);

    std::cout << " start audio" << std::endl;

    if (!m_driver->start(m_bufSize))
    {
        std::cerr << "Error starting" << std::endl;
        return;
    }
}

void Looper::stop()
{
    if (!m_driver->stop())
    {
        std::cerr << "Error stopping" << std::endl;
        return;
    }
    std::cout << " done" << std::endl;
}

void Looper::on_buffer(float *output, const float *input, const unsigned int &frames, const double &time)
{
    // std::cout << "Looper on_buffer"
    //           << " frames=" << frames
    //           << " time=" << time
    //           << std::endl;

    // /2 because 2 channels in this buffer
    // and transport assumes bar lengths in samples for 1 channel
    const auto ts = m_transport->update(frames / 2, time);

    Buffer in;
    Buffer out;
    size_t i = 0, j = 0, k = 0;
    // +=2 becaues 2 channels in this buffer
    // and transport assumes bar lengths in samples for 1 channel
    for (i = 0; i < frames; i += 2)
    {
        in.push_back(input[i]);
        in.push_back(input[i + 1]);

        if (ts.bar_frames.contains(i))
        {
            m_transport->bar().emit(ts.bar);
        }
        if (ts.beat_frames.contains(i))
        {
            out = m_mixer->process(in);
            for (j = 0; j < out.size(); ++j)
            {
                output[k++] = out[j];
            }
            in.clear();

            m_transport->beat().emit(ts.beat);
            s_transport.emit(ts.bar, ts.beat);
        }
    }

    out = m_mixer->process(in);
    for (j = 0; j < out.size(); ++j)
    {
        output[k++] = out[j];
    }
}
