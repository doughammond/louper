/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <deque>
#include <functional>
#include <map>
#include <memory>
#include <tuple>
#include <vector>
#include <sigc++/sigc++.h>

using Buffer = std::vector<float>;

enum STATE
{
    PLAY,
    REPLACE,
    OVERDUB,
    CLEAR
};

std::string StateName(STATE state);

using StateEdge = std::pair<STATE, STATE>;
using StateTarget = std::tuple<STATE, std::function<bool()>, std::function<void()>>; // target, cond(), action()
using StateTargetList = std::vector<StateTarget>;
using StateTransitionMap = std::map<StateEdge, StateTargetList>;

using SignalState = sigc::signal<void(const STATE &)>;
using SignalChannelState = sigc::signal<void(const uint &, const STATE &)>;
using SignalChannelPtr = sigc::signal<void(const uint &, const uint &)>;

/**
 * @brief Base interface type for all channels
 */
class IChannel
{
public:
    /**
     * @brief Shared pointer type for IChannel interface
     *
     */
    using sptr = std::shared_ptr<IChannel>;

    /**
     * @brief Process a period of audio; accept input, produce output
     *
     * @param input
     * @return Buffer
     */
    virtual Buffer process(const Buffer &input) = 0;
};

/**
 * @brief Passthrough (monitor) channel type
 */
class Passthrough : public IChannel
{
public:
    /**
     * @brief Process a period of audio; accept input, produce output
     *
     * @param input
     * @return Buffer
     */
    Buffer process(const Buffer &input);
};

/**
 * @brief Metronome generator channel type
 */
class Metronome : public IChannel
{
public:
    /**
     * @brief Construct a new Metronome object
     *
     * @param samplerate
     */
    explicit Metronome(const uint32_t &samplerate);

    /**
     * @brief Process a period of audio; accept input, produce output
     *
     * @param input
     * @return Buffer
     */
    Buffer process(const Buffer &input);

    /**
     * @brief Slot: Transport clock beat event.
     * Fills the audio buffer with a short beep.
     *
     * @param beat
     */
    void on_beat(uint beat);

private:
    /**
     * @brief samplerate
     */
    uint32_t m_fs;

    /**
     * @brief Audio buffer
     */
    std::deque<float> m_buf;
};

/**
 * @brief Loop recorder channel type
 */
class LoopRecorder : public IChannel
{
public:
    /**
     * @brief Shared pointer type for LoopRecorder
     *
     */
    using sptr = std::shared_ptr<LoopRecorder>;

    /**
     * @brief Construct a new Loop Recorder object
     *
     * @param id
     */
    explicit LoopRecorder(const uint &id);
    ~LoopRecorder();

    /**
     * @brief Command the state state machine using current cue state
     *
     * @return true
     * @return false
     */
    bool nextState();

    /**
     * @brief Command the cue state state machine
     *
     * @param command
     * @return true
     * @return false
     */
    bool nextCueState(const STATE &command);

    /**
     * @brief Slot: Transport clock bar changed
     *
     * @param bar
     */
    void on_bar(uint bar)
    {
        nextState();
    }

    /**
     * @brief Process a period of audio; accept input, produce output
     *
     * @param input
     * @return Buffer
     */
    Buffer process(const Buffer &input);

    /**
     * @brief Getter for cue state change signal
     */
    SignalChannelState &cue_change()
    {
        return s_cue;
    }

    /**
     * @brief Getter for state change signal
     */
    SignalChannelState &state_change()
    {
        return s_state;
    }

    /**
     * @brief Getter for buffer length change signal
     */
    SignalChannelPtr &length()
    {
        return s_length;
    }

    /**
     * @brief Getter for buffer position change signal
     */
    SignalChannelPtr &position()
    {
        return s_position;
    }

private:
    /**
     * @brief Channel ID (global index)
     */
    uint m_id;

    /**
     * @brief State change state machine transitions
     */
    StateTransitionMap CurrentStateTransitions;

    /**
     * @brief Cue state state machine transitions
     */
    StateTransitionMap CueStateTransitions;

    /**
     * @brief State change action: Entering REPLACE state
     */
    void on_enter_REPLACE();

    /**
     * @brief State change action: Entering PLAY state
     */
    void on_enter_PLAY();

    /**
     * @brief Loop state
     */
    STATE m_current_state = STATE::CLEAR;

    /**
     * @brief Cue state
     */
    STATE m_cue_state = STATE::CLEAR;

    /**
     * @brief Loop buffer
     */
    Buffer m_buf;

    /**
     * @brief Pointer into loop buffer
     */
    size_t m_bufptr = 0;

    /**
     * @brief Static empty buffer to return in case of no output
     */
    Buffer m_nullbuf;

    /**
     * @brief Signal: cue state changed
     */
    SignalChannelState s_cue;

    /**
     * @brief Signal: state changed
     */
    SignalChannelState s_state;

    /**
     * @brief Signal: buffer length changed
     */
    SignalChannelPtr s_length;

    /**
     * @brief Signal: buffer position changed
     */
    SignalChannelPtr s_position;
};
