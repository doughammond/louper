/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <tgmath.h>
#include <iostream>

#include "meter.h"

Meter::Meter(const int &bufferSize, const uint &samplerate)
    : sum_sqr(0.f),
      m_rbufptr(0)
{
    reset(bufferSize, samplerate);
}

Meter::~Meter()
{
}

void Meter::update(const std::vector<float> &buffer)
{
    for (size_t i = 0; i < buffer.size(); ++i)
    {
        const float absval = fabs(buffer[i]);

        levels.peak -= peakDecay;
        if (absval > levels.peak)
        {
            levels.peak = absval;
        }
        if (levels.peak < 0)
        {
            levels.peak = 0;
        }

        // subtract oldest sqr from sum
        sum_sqr -= sample_sqr_history[m_rbufptr];

        // calculate and accumulate new value
        sample_sqr_history[m_rbufptr] = absval * absval;
        sum_sqr += sample_sqr_history[m_rbufptr];

        m_rbufptr = (m_rbufptr + 1) % m_rbuflen;
    }

    levels.peakdB = dbClamp(10.f * log10(levels.peak));
    levels.RMS = sqrt(sum_sqr / m_rbuflen);
    levels.RMSdB = dbClamp(10.f * log10(levels.RMS));
}

void Meter::reset(const int &bufferSize, const uint &samplerate)
{
    m_rbuflen = bufferSize;
    sample_sqr_history.clear();
    sample_sqr_history.resize(bufferSize);
    std::fill(sample_sqr_history.begin(), sample_sqr_history.end(), 0);
    sum_sqr = 0.f;
    m_rbufptr = 0;

    peakDecay = 0.35f / samplerate;

    levels.peak = 0;
    levels.peakdB = DB_MIN;
    levels.RMS = 0;
    levels.RMSdB = DB_MIN;
}

float Meter::dbClamp(const float &db)
{
    if (db < DB_MIN)
        return DB_MIN;
    if (db > 0.f)
        return 0;
    return db;
}
