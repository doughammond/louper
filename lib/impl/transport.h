/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <memory>
#include <stdint.h>
#include <unordered_set>
#include <sigc++/sigc++.h>

#include "taptempo.h"

using SignalTimeSig = sigc::signal<void(const uint &, const uint &)>;
using SignalClock = sigc::signal<void(const uint &)>;
using SignalBarLength = sigc::signal<void(const uint32_t &)>;

/**
 * @brief Buffer boundary calculator
 *
 * The buffer length provided by the audio driver and the length
 * of a bar or beat in terms of buffer elements. During the processing
 * of an audio driver buffer, we may need to change application
 * states at the points where bars and beats occur. This calculator
 * will derive the indexes into each audio buffer at which these
 * events need to occur.
 */
struct TransportBufBoundaries
{

  /**
   * @brief Number of period (right-hand) buffers which have elapsed
   */
  uint64_t i;

  /**
   * @brief Number of period (right-hand) buffer elements which have elapsed
   */
  uint64_t cnt;

  /**
   * @brief Number of timing (left-hand) buffer elements which have elapsed
   */
  uint64_t ptr;

  /**
   * @brief Number of elements to carry over to the next period
   */
  uint64_t accum;

  /**
   * @brief Reset all counters
   *
   */
  void reset()
  {
    i = 0;
    cnt = 0;
    ptr = 0;
    accum = 0;
  }

  /**
   * @brief
   *  Given a timing buffer lenfth (left-hand) and a period buffer length (right-hand),
   *  calculate the offset into period buffer at which the timing buffer boundaries occur.
   *
   * @param beatlen
   * @param bufsize
   * @return std::unordered_set<uint>
   */
  std::unordered_set<uint> next(const uint beatlen, const uint bufsize)
  {
    std::unordered_set<uint> offs;
    if (accum > bufsize)
    {
      accum -= bufsize;
    }
    else if (accum > 0)
    {
      offs.insert(accum);
      accum = 0;
    }
    cnt += bufsize;
    while (ptr < cnt)
    {
      ptr += beatlen;
      if (ptr <= cnt)
      {
        const auto off = ptr - i * bufsize;
        offs.insert(off);
        accum = 0;
      }
      else
      {
        accum = ptr - cnt;
      }
    }
    i++;
    return offs;
  }
};

/**
 * @brief Transport clock state
 */
struct TransportFrameState
{
  /**
   * @brief Indexes of current period buffer at which the bar changes
   *
   */
  std::unordered_set<uint> bar_frames;

  /**
   * @brief Indexes of current period buffer at which the beat changes
   *
   */
  std::unordered_set<uint> beat_frames;

  /**
   * @brief Current time: bar
   *
   */
  uint bar;

  /**
   * @brief Current time: beat
   *
   */
  uint beat;
};

/**
 * @brief Transport / time controller
 */
class Transport
{
public:
  /**
   * @brief Shared pointer type for Transport
   *
   */
  using sptr = std::shared_ptr<Transport>;

  explicit Transport();
  ~Transport();

  /**
   * @brief Reset transport state
   *
   */
  void reset();

  /**
   * @brief Setter for samplerate
   *
   * @param fs
   */
  void samplerate(const uint32_t &fs);

  /**
   * @brief Slot: set tempo
   *
   * @param bpm
   */
  void on_bpm(const float &bpm);

  /**
   * @brief Slot: set time signature
   *
   * @param num
   * @param den
   */
  void on_timesig(const uint &num, const uint &den);

  /**
   * @brief Main transport update function; called from audio period process
   *
   * @param frames
   * @param time
   * @return TransportFrameState
   */
  TransportFrameState update(const uint &frames, const double &time);

  /**
   * @brief Getter for tempo signal
   *
   * @return SignalTempo&
   */
  SignalTempo &tempo()
  {
    return s_tempo;
  }

  /**
   * @brief Getter for time signature signal
   *
   * @return SignalTimeSig&
   */
  SignalTimeSig &timesig()
  {
    return s_timesig;
  }

  /**
   * @brief Getter for bar length signal
   *
   * @return SignalBarLength&
   */
  SignalBarLength &bar_length()
  {
    return s_bar_length;
  }

  /**
   * @brief Getter for clock bar signal
   *
   * @return SignalClock&
   */
  SignalClock &bar()
  {
    return s_bar;
  }

  /**
   * @brief Getter for clock beat signal
   *
   * @return SignalClock&
   */
  SignalClock &beat()
  {
    return s_beat;
  }

private:
  /**
   * @brief Calculate values derived from samplerate, tempo and time signature
   */
  void update_params();

  /**
   * @brief Samplerate
   */
  uint32_t m_fs;

  /**
   * @brief User requested
   */
  float m_bpm;

  /**
   * @brief time signature numerator
   */
  uint m_ts_num;

  /**
   * @brief time signature denominator
   */
  uint m_ts_den;

  /**
   * @brief Calculated beat length in frames
   */
  uint32_t m_beat_len;

  /**
   * @brief Calculated bar length in frames
   */
  uint32_t m_bar_len;

  /**
   * @brief Clock: current bar
   */
  uint m_bar;

  /**
   * @brief Clock: current beat
   */
  uint m_beat;

  /**
   * @brief Bar boundary detector
   */
  TransportBufBoundaries m_bar_boundaries;

  /**
   * @brief: beat boundary detector
   */
  TransportBufBoundaries m_beat_boundaries;

  /**
   * @brief Signal: tempo changed
   */
  SignalTempo s_tempo;

  /**
   * @brief Signal: time signature changed
   */
  SignalTimeSig s_timesig;

  /**
   * @brief Signal: bar buffer length changed
   */
  SignalBarLength s_bar_length;

  /**
   * @brief: Signal clock bar changed
   */
  SignalClock s_bar;

  /**
   * @brief: Signal clock beat changed
   */
  SignalClock s_beat;
};
