/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>
#include <lo/lo.h>
#include <lo/lo_cpp.h>

#include "control.h"

Control::Control(const uint &channels)
    : m_channels(channels),
      m_timesigs({
          {4, 4},
          {3, 4},
          {6, 8},
      }),
      m_control_cue_ptr(0),
      m_control_cue(STATE::REPLACE),
      m_timesig_idx(0)
{
    std::shared_ptr<lo::ServerThread> p = std::make_shared<lo::ServerThread>(9000);

    p->add_method(
        "cue", "i",
        [this](lo_arg **argv, int argc)
        {
            if (argc == 1)
            {
                auto i = argv[0]->i;
                std::cout << "cue " << i << std::endl;
                if (i >= 0 && i < m_channels)
                {
                    s_cue.emit(i, m_control_cue);
                }
            }
        });

    p->add_method(
        "mute", "iT",
        [this](lo_arg **argv, int argc)
        {
            if (argc == 2)
            {
                auto i = argv[0]->i;
                std::cout << "mute T " << i << std::endl;
                if (i >= 0 && i < m_channels)
                {
                    s_mute.emit(i, true);
                }
            }
        });

    p->add_method(
        "mute", "iF",
        [this](lo_arg **argv, int argc)
        {
            if (argc == 2)
            {
                auto i = argv[0]->i;
                std::cout << "mute F " << i << std::endl;
                if (i >= 0 && i < m_channels)
                {
                    s_mute.emit(i, false);
                }
            }
        });

    p->add_method(
        "solo", "iT",
        [this](lo_arg **argv, int argc)
        {
            if (argc == 2)
            {
                auto i = argv[0]->i;
                std::cout << "solo T " << i << std::endl;
                if (i >= 0 && i < m_channels)
                {
                    s_solo.emit(i, true);
                }
            }
        });

    p->add_method(
        "solo", "iF",
        [this](lo_arg **argv, int argc)
        {
            if (argc == 2)
            {
                auto i = argv[0]->i;
                std::cout << "solo F " << i << std::endl;
                if (i >= 0 && i < m_channels)
                {
                    s_solo.emit(i, false);
                }
            }
        });

    p->add_method(
        "mode", "",
        [this](lo_arg **argv, int)
        {
            m_control_cue_ptr = (m_control_cue_ptr + 1) % 3;
            m_control_cue = ControlStates[m_control_cue_ptr];
            s_control_cue.emit(m_control_cue);
        });

    p->add_method(
        "taptempo", "",
        [this](lo_arg **argv, int)
        {
            s_taptempo.emit();
        });

    p->add_method(
        "timesig", "",
        [this](lo_arg **argv, int)
        {
            m_timesig_idx = (m_timesig_idx + 1) % m_timesigs.size();
            std::cout << "timesig " << m_timesig_idx
                      << " -> " << m_timesigs[m_timesig_idx].first << "/" << m_timesigs[m_timesig_idx].second
                      << std::endl;
            s_timesig.emit(m_timesigs[m_timesig_idx].first, m_timesigs[m_timesig_idx].second);
        });

    p->add_method(
        "clearall", "",
        [this](lo_arg **argv, int)
        {
            for (uint i = 0; i < m_channels; ++i)
            {
                s_cue.emit(i, STATE::CLEAR);
            }
        });

    p->start();

    m_drvimpl = std::static_pointer_cast<void>(p);
}

Control::~Control()
{
}
