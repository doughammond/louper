/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include "audiodrv.h"
#include <RtAudio.h>

AudioDrv::AudioDrv(const uint32_t &samplerate)
    : m_fs(samplerate)
{
}

AudioDrv::~AudioDrv()
{
    stop();
}

int RTCallback(
    void *outputBuffer, void *inputBuffer,
    unsigned int nBufferFrames, double streamTime,
    RtAudioStreamStatus status,
    void *pdrv)
{
    if (status == RTAUDIO_INPUT_OVERFLOW)
    {
        std::cerr << "AudioDrv RTCallback input overflow at t=" << streamTime << std::endl;
    }

    if (status == RTAUDIO_OUTPUT_UNDERFLOW)
    {
        std::cerr << "AudioDrv RTCallback output underflow at t=" << streamTime << std::endl;
    }

    AudioDrv *drv = (AudioDrv *)pdrv;
    drv->process(
        static_cast<float *>(outputBuffer),
        static_cast<float *>(inputBuffer),
        // *2  because of 2 channels interleaved
        nBufferFrames * 2, streamTime);

    return 0;
}

bool AudioDrv::start(const uint &bufsize)
{
    if (m_drvimpl.get())
    {
        std::cerr << "AudioDrv start error; already started" << std::endl;
        return false;
    }

    std::shared_ptr<RtAudio> p = std::make_shared<RtAudio>();

    const auto devCount = p->getDeviceCount();
    if (devCount < 1)
    {
        std::cerr << "AudioDrv start error; no available device" << std::endl;
        return false;
    }

    unsigned int bufferBytes, bufferFrames = bufsize;
    RtAudio::StreamParameters iParams, oParams;
    iParams.deviceId = 0; // first
    iParams.nChannels = 2;
    oParams.deviceId = 0;  // first
    oParams.nChannels = 2; // need 2 eventually

    auto openres = p->openStream(
        &oParams, &iParams,
        RTAUDIO_FLOAT32, m_fs,
        &bufferFrames, &RTCallback,
        static_cast<void *>(this));
    if (openres != RTAUDIO_NO_ERROR)
    {
        std::cerr << "AudioDrv start error (" << openres << "); cannot open stream" << std::endl;
        return false;
    }

    auto startres = p->startStream();
    if (startres != RTAUDIO_NO_ERROR)
    {
        std::cerr << "AudioDrv start error (" << startres << "); cannot start stream" << std::endl;
        return false;
    }

    m_drvimpl = std::static_pointer_cast<void>(p);
    return true;
}

bool AudioDrv::stop()
{
    if (m_drvimpl.get())
    {
        auto p = std::static_pointer_cast<RtAudio>(m_drvimpl);
        if (p->isStreamOpen())
        {
            p->stopStream();
        }
        m_drvimpl.reset();
    }

    return true;
}
