/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>
#include <tgmath.h>

#include "channel.h"

constexpr double TWOPI = 2 * 3.141592;

Buffer Passthrough::process(const Buffer &input)
{
    return input;
}

Metronome::Metronome(const uint32_t &samplerate)
    : m_fs(samplerate)
{
}

Buffer Metronome::process(const Buffer &input)
{
    Buffer b;
    b.resize(input.size());
    for (size_t i = 0; i < input.size(); i += 2)
    {
        if (!m_buf.empty())
        {
            b[i] = m_buf.front();
            m_buf.pop_front();
        }
    }
    return b;
}

void Metronome::on_beat(uint beat)
{
    m_buf.clear();

    auto f = 1320.f;
    if (beat == 1)
    {
        f *= 3. / 2;
    }
    for (size_t i = 0; i < m_fs / 60.f; ++i)
    {
        m_buf.push_back(0.8 * sin(TWOPI * f * (i / (float)m_fs)));
    }
}

std::string StateName(STATE state)
{
    switch (state)
    {
    case STATE::PLAY:
        return "PLAY";
        break;
    case STATE::REPLACE:
        return "REPLACE";
        break;
    case STATE::OVERDUB:
        return "OVERDUB";
        break;
    case STATE::CLEAR:
        return "CLEAR";
        break;

    default:
        return "UNKNOWN";
        break;
    }
}

bool defaultCond()
{
    return true;
}

void nullAction()
{
    // no-op
}

LoopRecorder::LoopRecorder(const uint &id)
    : m_id(id)
{
    /*
    Next current state derivation.

    This state change is requested at the end of each transport bar period.
    The Channel may execute the change or not depending on it's loop contents.

    Cue State       Current State

                    PLAY        REPLACE         OVERDUB         CLEAR
    ------------------------------------------------------------------
    PLAY            -           PLAY            PLAY            -
    REPLACE         REPLACE     -               REPLACE         REPLACE
    OVERDUB         OVERDUB     OVERDUB         -               REPLACE
    CLEAR           CLEAR       CLEAR           CLEAR           -

    */
    // { CURRENT, CUE } => { NEXT CURRENT }
    CurrentStateTransitions = {
        // column 1
        {{STATE::PLAY, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, std::bind(&LoopRecorder::on_enter_REPLACE, this)}}},
        {{STATE::PLAY, STATE::OVERDUB}, {{STATE::OVERDUB, defaultCond, nullAction}}},
        {{STATE::PLAY, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // column 2
        {{STATE::REPLACE, STATE::PLAY}, {{STATE::PLAY, defaultCond, std::bind(&LoopRecorder::on_enter_PLAY, this)}}},
        {{STATE::REPLACE, STATE::OVERDUB}, {{STATE::OVERDUB, defaultCond, nullAction}}},
        {{STATE::REPLACE, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // column 3
        {{STATE::OVERDUB, STATE::PLAY}, {{STATE::PLAY, defaultCond, std::bind(&LoopRecorder::on_enter_PLAY, this)}}},
        {{STATE::OVERDUB, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, std::bind(&LoopRecorder::on_enter_REPLACE, this)}}},
        {{STATE::OVERDUB, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // column 4
        {{STATE::CLEAR, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, std::bind(&LoopRecorder::on_enter_REPLACE, this)}}},
        {{STATE::CLEAR, STATE::OVERDUB}, {{STATE::REPLACE, defaultCond, std::bind(&LoopRecorder::on_enter_REPLACE, this)}}},
    };

    /*
    Next cue state derivation. NB "PLAY" is not a command.

    This state change is executed immediately when the command is requested.

    Command         Cue State

                    PLAY        REPLACE         OVERDUB         CLEAR
    ------------------------------------------------------------------
    REPLACE         REPLACE     PLAY/CLEAR      REPLACE         REPLACE
    OVERDUB         OVERDUB     OVERDUB         PLAY/CLEAR      REPLACE
    CLEAR           CLEAR       CLEAR           CLEAR           -

    */
    // { CUE, COMMAND } => { NEXT CUE }
    CueStateTransitions = {
        // Column 1
        {{STATE::PLAY, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, nullAction}}},
        {{STATE::PLAY, STATE::OVERDUB}, {{STATE::OVERDUB, defaultCond, nullAction}}},
        {{STATE::PLAY, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // Column 2
        {{STATE::REPLACE, STATE::REPLACE},
         {
             {STATE::PLAY,
              [this]()
              {
                  return m_current_state != STATE::CLEAR;
              },
              nullAction},
             {STATE::CLEAR,
              [this]()
              {
                  return m_current_state == STATE::CLEAR;
              },
              nullAction},
         }},
        {{STATE::REPLACE, STATE::OVERDUB},
         {
             {STATE::OVERDUB,
              [this]()
              {
                  return m_current_state != STATE::CLEAR;
              },
              nullAction},
             {STATE::CLEAR,
              [this]()
              {
                  return m_current_state == STATE::CLEAR;
              },
              nullAction},
         }},
        {{STATE::REPLACE, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // Column 3
        {{STATE::OVERDUB, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, nullAction}}},
        {{STATE::OVERDUB, STATE::OVERDUB},
         {
             {STATE::PLAY, [this]()
              {
                  return m_current_state != STATE::CLEAR;
              },
              nullAction},
             {STATE::CLEAR, [this]()
              {
                  return m_current_state == STATE::CLEAR;
              },
              nullAction},
         }},
        {{STATE::OVERDUB, STATE::CLEAR}, {{STATE::CLEAR, defaultCond, nullAction}}},

        // Column 4
        {{STATE::CLEAR, STATE::REPLACE}, {{STATE::REPLACE, defaultCond, nullAction}}},
        {{STATE::CLEAR, STATE::OVERDUB}, {{STATE::REPLACE, defaultCond, nullAction}}},
    };
}

LoopRecorder::~LoopRecorder()
{
}

bool LoopRecorder::nextState()
{
    // This is called at the end of every transport bar period.
    // This channel st decide whether to action this change now
    // (in the case this channel is currently empty or at the end of its current loop)
    // or not
    // (in case this channel is not empty and only part way through its loop)
    // TODO: state interrupt modes - wait for bar or wait for loop end?
    const auto tr = CurrentStateTransitions.find({m_current_state, m_cue_state});
    if (tr != CurrentStateTransitions.end())
    {
        for (const auto &[target, cond, action] : tr->second)
        {
            if (cond())
            {
                m_current_state = target;
                action();
                s_state.emit(m_id, m_current_state);
                std::cout << "  LoopRecorder[" << m_id << "]::nextState changed " << m_current_state << std::endl;
                return true;
            }
        }
    }

    return false;
}

bool LoopRecorder::nextCueState(const STATE &command)
{
    const auto tr = CueStateTransitions.find({m_cue_state, command});
    if (tr != CueStateTransitions.end())
    {
        for (const auto &[target, cond, action] : tr->second)
        {
            if (cond())
            {
                m_cue_state = target;
                action();
                s_cue.emit(m_id, m_cue_state);
                return true;
            }
        }
    }

    return false;
}

Buffer LoopRecorder::process(const Buffer &input)
{
    m_nullbuf.resize(input.size());
    // std::cout << "  LoopRecorder[" << m_id << "]::process input len=" << input.size() << std::endl;
    switch (m_current_state)
    {
    case STATE::PLAY:
    {
        // std::cout << "  LoopRecorder[" << m_id << "]::process state PLAY "  << m_buf.size() << std::endl;
        Buffer n;
        const auto is = input.size();
        const auto bs = m_buf.size();
        // TODO: can this be done without element-wise iteration?
        uint ptr = 0;
        for (size_t i = 0; i < is; ++i)
        {
            ptr = (m_bufptr++) % bs;
            n.push_back(m_buf[ptr]);
        }
        s_position.emit(m_id, ptr);
        return n;
    }
    break;
    case STATE::REPLACE:
    {
        m_buf.insert(std::end(m_buf), std::begin(input), std::end(input));
        // std::cout <<  "  LoopRecorder[" << m_id << "]::process REPLACE " << m_buf.size() << std::endl;
        s_length.emit(m_id, m_buf.size());
        s_position.emit(m_id, m_buf.size());
        return m_nullbuf;
    }
    break;
    case STATE::OVERDUB:
    {
        // // std::cout << "  LoopRecorder[" << m_id << "]::process state OVERDUB" << std::endl;
        Buffer n;
        const auto is = input.size();
        const auto bs = m_buf.size();
        // TODO: can this be done without element-wise iteration?
        uint ptr = 0;
        for (size_t i = 0; i < is; ++i)
        {
            ptr = (m_bufptr++) % bs;
            n.push_back(m_buf[ptr]); // play out the old value
            m_buf[ptr] += input[i];  // overdub the input valuw
        }
        s_position.emit(m_id, ptr);
        return n;
    }
    break;
    case STATE::CLEAR:
    {
        // std::cout << "  LoopRecorder[" << m_id << "]::process state CLEAR" << std::endl;
        return m_nullbuf;
    }
    break;
    }

    return m_nullbuf;
}

void LoopRecorder::on_enter_REPLACE()
{
    // std::cout << "  LoopRecorder[" << m_id << "]::on_enter_REPLACE" << std::endl;
    m_buf.clear();
    m_bufptr = 0;
}

void LoopRecorder::on_enter_PLAY()
{
    // std::cout << "  LoopRecorder[" << m_id << "]::on_enter_PLAY" << std::endl;
    m_bufptr = 0;
}
