/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <unordered_map>
#include <sigc++/sigc++.h>

#include "channel.h"
#include "meter.h"

// For Mute and Solo per channel
using MixerChannelStateMap = std::unordered_map<uint, bool>;

// (int channelId, Meterlevels levels);
// channelId < 0 -> Outputs
// channelId >=0 -> Inputs
// idx, levels, mute, solo
using SignalLevels = sigc::signal<void(const uint &, const MeterLevels &, const bool &, const bool &)>;

/**
 * @brief Audio mixer
 */
class Mixer
{
public:
    /**
     * @brief Shared pointer type for Mixer
     *
     */
    using sptr = std::shared_ptr<Mixer>;

    /**
     * @brief Construct a new Mixer object
     *
     * @param channels
     * @param samplerate
     */
    explicit Mixer(std::vector<IChannel::sptr> channels, const uint &samplerate);
    ~Mixer();

    /**
     * @brief Process a period of audio; accept input, produce output
     *
     * @param input
     * @return Buffer
     */
    Buffer process(const Buffer &input);

    /**
     * @brief Slot: Transport clock bar changed
     *
     * @param bar
     */
    void on_bar(const uint &bar);

    /**
     * @brief Set the mute cue state for a channel
     *
     * @param channel
     * @param val
     */
    void cue_mute(const uint &channel, const bool &val)
    {
        m_channel_mute_cue[channel] = val;
    }

    /**
     * @brief Set the solo cue state for a channel
     *
     * @param channel
     * @param val
     */
    void cue_solo(const uint &channel, const bool &val)
    {
        m_channel_solo_cue[channel] = val;
    }

    /**
     * @brief Getter for audio levels signal
     *
     * @return SignalLevels&
     */
    SignalLevels &levels()
    {
        return s_levels;
    }

private:
    /**
     * @brief Audio level meters for inputs
     */
    std::vector<Meter::sptr> m_input_meters;

    /**
     * @brief Audio level meter for output
     */
    Meter::sptr m_output_meter;

    /**
     * @brief Signal: Audio levels for a channel
     */
    SignalLevels s_levels;

    /**
     * @brief Channels to be mixed
     *
     */
    std::vector<IChannel::sptr> m_channels;

    /**
     * @brief Map of channel mute cue states
     */
    MixerChannelStateMap m_channel_mute_cue;

    /**
     * @brief Map of channel mute states
     */
    MixerChannelStateMap m_channel_mute;

    /**
     * @brief Map of channel solo cue states
     */
    MixerChannelStateMap m_channel_solo_cue;

    /**
     * @brief Map of channel solo states
     */
    MixerChannelStateMap m_channel_solo;

    /**
     * @brief Is any channel in solo state?
     *
     */
    bool m_any_solo;
};
