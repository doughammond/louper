/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>
#include <tgmath.h>

#include "transport.h"

Transport::Transport()
{
}

Transport::~Transport()
{
}

void Transport::reset()
{
    m_bar = 0;
    m_beat = 0;
    m_bar_boundaries.reset();
    m_beat_boundaries.reset();
}

void Transport::samplerate(const uint32_t &fs)
{
    m_fs = fs;
    update_params();
}

void Transport::on_bpm(const float &bpm)
{
    m_bpm = bpm;
    update_params();
}

void Transport::on_timesig(const uint &num, const uint &den)
{
    m_ts_num = num;
    m_ts_den = den;
    update_params();
}

TransportFrameState Transport::update(const uint &frames, const double &time)
{
    TransportFrameState ts{
        .bar_frames = m_bar_boundaries.next(m_bar_len, frames),
        .beat_frames = m_beat_boundaries.next(m_beat_len, frames),
    };

    m_bar += ts.bar_frames.size();
    m_beat = (m_beat + ts.beat_frames.size()) % m_ts_num;

    ts.bar = 1 + m_bar;
    ts.beat = 1 + m_beat;

    return ts;
}

void Transport::update_params()
{
    if (m_fs == 0 || m_bpm == 0 || m_ts_num == 0 || m_ts_den == 0)
    {
        return;
    }

    auto beatperiod = 60.0 / m_bpm * (4.0 / m_ts_den);
    m_beat_len = beatperiod * m_fs;

    // must be even
    while (m_beat_len % 2 != 0)
    {
        m_beat_len++;
    }

    m_bar_len = m_beat_len * m_ts_num;
    m_bar = 0;
    m_beat = 0;
    m_bar_boundaries.reset();
    m_beat_boundaries.reset();

    std::cout << "Transport params"
              << " fs=" << m_fs
              << " beat period=" << beatperiod
              << " beat_frames=" << m_beat_len
              << " bar_frames=" << m_bar_len
              << std::endl;

    s_tempo.emit(m_bpm);
    s_timesig.emit(m_ts_num, m_ts_den);
    s_bar_length.emit(m_bar_len * 2);
}
