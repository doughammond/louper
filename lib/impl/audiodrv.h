/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <memory>
#include <sigc++/sigc++.h>

// float *output, float *input, unsigned int frames, double time
using SignalBuffer = sigc::signal<void(float *, const float *, const unsigned int &, const double &)>;

/**
 * @brief Audio driver controller
 */
class AudioDrv
{
public:
    /**
     * @brief Shared pointer type for AudioDrv
     *
     */
    using sptr = std::shared_ptr<AudioDrv>;

    /**
     * @brief Construct a new Audio Drv object
     *
     * @param samplerate
     */
    explicit AudioDrv(const uint32_t &samplerate);
    ~AudioDrv();

    /**
     * @brief Audio driver control: Start
     *
     * @param bufsize
     * @return true
     * @return false
     */
    bool start(const uint &bufsize);

    /**
     * @brief Audio driver control: Stop
     *
     * @return true
     * @return false
     */
    bool stop();

    /**
     * @brief Audio driver buffer process callback
     *
     * @param output
     * @param input
     * @param frames
     * @param time
     */
    void process(float *output, const float *input, const unsigned int &frames, const double &time)
    {
        s_buffer.emit(output, input, frames, time);
    }

    /**
     * @brief Getter for sample rate
     *
     * @return uint32_t
     */
    uint32_t samplerate() const
    {
        return m_fs;
    }

    /**
     * @brief Getter for audio buffer ready signal
     *
     * @return SignalBuffer&
     */
    SignalBuffer &buffer()
    {
        return s_buffer;
    }

private:
    /**
     * @brief Sameple rate
     */
    uint32_t m_fs;

    /**
     * @brief Audio driver implementation
     */
    std::shared_ptr<void> m_drvimpl;

    /**
     * @brief Signal for buffer ready to process
     */
    SignalBuffer s_buffer;
};
