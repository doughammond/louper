/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <sigc++/sigc++.h>

#include "audiodrv.h"
#include "channel.h"
#include "control.h"
#include "mixer.h"
#include "taptempo.h"
#include "transport.h"

using SignalTransportClock = sigc::signal<void(const uint &, const uint &)>;

/**
 * @brief Looper application controller
 */
class Looper
{
public:
    /**
     * @brief Shared pointer type for Looper
     *
     */
    using sptr = std::shared_ptr<Looper>;

    /**
     * @brief Construct a new Looper object
     *
     * @param channelCount
     * @param samplerate
     * @param bufSize
     */
    explicit Looper(const size_t &channelCount, const uint32_t &samplerate, const uint &bufSize);
    ~Looper();

    /**
     * @brief Program control: Start
     */
    void start();

    /**
     * @brief Program control: Stop
     */
    void stop();

    /**
     * @brief Slot: Audio driver buffer handler
     *
     * @param output
     * @param input
     * @param frames
     * @param time
     */
    void on_buffer(float *output, const float *input, const unsigned int &frames, const double &time);

    /**
     * @brief Getter for forwaded signal: Transport clock
     */
    SignalTransportClock &transport_state()
    {
        return s_transport;
    }

    /**
     * @brief Getter for forwaded signal: Transport tempo
     */
    SignalTempo &tempo()
    {
        return s_tempo;
    }

    /**
     * @brief Getter for forwaded signal: Transport time signature
     */
    SignalTimeSig &timesig()
    {
        return s_timesig;
    }

    /**
     * @brief Getter for forwaded signal: Transport bar buffer length
     */
    SignalBarLength &bar_length()
    {
        return s_bar_length;
    }

    /**
     * @brief Getter for forwaded signal: Channel cue state
     */
    SignalChannelState &channel_cue()
    {
        return s_channel_cue;
    }

    /**
     * @brief Getter for forwaded signal: Channel state
     */
    SignalChannelState &channel_state()
    {
        return s_channel_state;
    }

    /**
     * @brief Getter for forwaded signal: Channel buffer length
     */
    SignalChannelPtr &channel_length()
    {
        return s_channel_length;
    }

    /**
     * @brief Getter for forwaded signal: Channel buffer position
     */
    SignalChannelPtr &channel_position()
    {
        return s_channel_position;
    }

    /**
     * @brief Getter for forwaded signal: Channel audio levels
     */
    SignalLevels &levels()
    {
        return s_levels;
    }

    /**
     * @brief Getter for forwaded signal: Control cue state
     */
    SignalState &control_cue()
    {
        return s_control_cue;
    }

private:
    /**
     * @brief Audio driver buffer size
     */
    uint m_bufSize;

    /**
     * @brief Audio IO driver
     */
    AudioDrv::sptr m_driver;

    /**
     * @brief Transport/clock controller
     */
    Transport::sptr m_transport;

    /**
     * @brief Remote control interface
     */
    Control::sptr m_control;

    /**
     * @brief Tap tempo calculator
     */
    TapTempo::sptr m_taptempo;

    /**
     * @brief Audio mixer
     */
    Mixer::sptr m_mixer;

    /**
     * @brief Loop recorder channels
     */
    std::vector<LoopRecorder::sptr> m_loop_channels;

    /**
     * @brief Forwarded signal: Transport clock
     */
    SignalTransportClock s_transport;

    /**
     * @brief Forwarded signal: Transport tempo
     */
    SignalTempo s_tempo;

    /**
     * @brief Forwarded signal: Transport time signature
     */
    SignalTimeSig s_timesig;

    /**
     * @brief Forwarded signal: Transport bar buffer length
     */
    SignalBarLength s_bar_length;

    /**
     * @brief Forwarded signal: Channel cue state
     */
    SignalChannelState s_channel_cue;

    /**
     * @brief Forwarded signal: Channel state
     */
    SignalChannelState s_channel_state;

    /**
     * @brief Forwarded signal: Channel buffer length
     */
    SignalChannelPtr s_channel_length;

    /**
     * @brief Forwarded signal: Channel buffer position
     */
    SignalChannelPtr s_channel_position;

    /**
     * @brief Forwarded signal: Channel audio levels
     */
    SignalLevels s_levels;

    /**
     * @brief Forwarded signal: Control cue state
     */
    SignalState s_control_cue;
};
