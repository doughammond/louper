/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>

#include "mixer.h"

Mixer::Mixer(std::vector<IChannel::sptr> channels, const uint &samplerate)
    : m_any_solo(false)
{
    m_channels = channels;

    for (size_t i = 0; i < channels.size(); ++i)
    {
        m_input_meters.push_back(std::move(std::make_shared<Meter>(4096, samplerate)));

        m_channel_mute[i] = false;
        m_channel_mute_cue[i] = false;
        m_channel_solo[i] = false;
        m_channel_solo_cue[i] = false;
    }

    m_output_meter = std::make_shared<Meter>(4096, samplerate);
}

Mixer::~Mixer()
{
}

Buffer Mixer::process(const Buffer &input)
{
    // std::cout << " Mixer::process"
    //           << " input=" << input.size()
    //           << std::endl;

    Buffer b, n;
    b.resize(input.size());
    n.resize(input.size());
    for (size_t j = 0; j < m_channels.size(); ++j)
    {
        // must update channel with process() regardless of mix output
        auto c = m_channels[j];
        const Buffer cout = c->process(input);

        if (m_channel_mute[j] || (m_any_solo && !m_channel_solo[j]))
        {
            m_input_meters[j]->update(n);
            s_levels.emit(
                j,
                m_input_meters[j]->levels,
                m_channel_mute[j],
                m_channel_solo[j]);
            continue;
        }

        m_input_meters[j]->update(cout);
        s_levels.emit(
            j,
            m_input_meters[j]->levels,
            m_channel_mute[j],
            m_channel_solo[j]);
        for (size_t i = 0; i < cout.size(); ++i)
        {
            b[i] += cout[i];
        }
    }
    m_output_meter->update(b);
    s_levels.emit(-1, m_output_meter->levels, false, false);
    return b;
}

void Mixer::on_bar(const uint &bar)
{
    // Copy cue states to live states
    for (const auto &[channel, mute] : m_channel_mute_cue)
    {
        m_channel_mute[channel] = mute;
    }

    m_any_solo = false;
    for (const auto &[channel, solo] : m_channel_solo_cue)
    {
        m_channel_solo[channel] = solo;
        m_any_solo |= solo;
    }
}
