/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <memory>
#include <vector>

/**
 * @brief Audio levels data container
 */
struct MeterLevels
{
    /**
     * @brief Signal level peak in range 0.0 - 1.0;
     */
    float peak;

    /**
     * @brief Signal level peak (dB) in range -Inf - 0.f
     */
    float peakdB;

    /**
     * @brief Signal level RMS in range 0.0 - 1.0;
     */
    float RMS;

    /**
     * @brief Signal level RMS (dB) in range -Inf - 0.f
     */
    float RMSdB;
};

/**
 * @brief Audio signal level calculator
 */
class Meter
{
public:
    /**
     * @brief Shared pointer type for Meter
     *
     */
    using sptr = std::shared_ptr<Meter>;

    /**
     * @brief Minimum acceptable dB level
     */
    static constexpr float DB_MIN = -30.f;

    /**
     * @brief Current meter levels
     */
    MeterLevels levels;

    /**
     * @brief Construct a new Meter object
     *
     * @param bufferSize
     * @param samplerate
     */
    explicit Meter(const int &bufferSize, const uint &samplerate);
    ~Meter();

    /**
     * @brief Update the peak and averaged levels with input signal
     *
     * @param buffer
     */
    void update(const std::vector<float> &buffer);

    /**
     * @brief Reset peak and averaged values
     *
     * @param bufferSize
     * @param samplerate
     */
    void reset(const int &bufferSize, const uint &samplerate);

private:
    /**
     * @brief Clamp a dB value into sensible range
     *
     * @param db
     * @return float
     */
    float dbClamp(const float &db);

    /**
     * @brief Averaging running sum
     */
    float sum_sqr;

    /**
     * @brief Averaging ring buffer
     */
    std::vector<float> sample_sqr_history;

    /**
     * @brief Averaging ring buffer length
     */
    int m_rbuflen;

    /**
     * @brief Averaging ring buffer pointer
     */
    int m_rbufptr;

    /**
     * @brief Peak value decay factor
     */
    float peakDecay;
};
