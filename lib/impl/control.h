/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <memory>
#include <vector>
#include <sigc++/sigc++.h>

#include "channel.h"

using SignalVoid = sigc::signal<void(void)>;

// TODO: duplicate of SignalTimesig
using SignalUInt2 = sigc::signal<void(const uint &, const uint &)>;
using SignalUintBool = sigc::signal<void(const uint &, const bool &)>;

constexpr STATE ControlStates[3] = {
    STATE::REPLACE,
    STATE::OVERDUB,
    STATE::CLEAR,
};

/**
 * @brief Remote control server controller
 */
class Control
{
public:
    /**
     * @brief Shared pointer type for Control
     *
     */
    using sptr = std::shared_ptr<Control>;

    /**
     * @brief Construct a new Control object
     *
     * @param channels
     */
    explicit Control(const uint &channels);
    ~Control();

    /**
     * @brief Getter for Channel cue state signal
     */
    SignalChannelState &channel_cue()
    {
        return s_cue;
    }

    /**
     * @brief Getter for Channel mute state signal
     */
    SignalUintBool &channel_mute()
    {
        return s_mute;
    }

    /**
     * @brief Getter for Channel solo state signal
     */
    SignalUintBool &channel_solo()
    {
        return s_solo;
    }

    /**
     * @brief Getter for Control cue state signal
     */
    SignalState &control_cue()
    {
        return s_control_cue;
    }

    /**
     * @brief Getter for Tap tempo input signal
     */
    SignalVoid &taptempo()
    {
        return s_taptempo;
    }

    /**
     * @brief Getter for Transport time signature signal
     */
    SignalUInt2 &timesig()
    {
        return s_timesig;
    }

private:
    /**
     * @brief Total number of channels
     */
    uint m_channels;

    /**
     * @brief Remote control driver implementation
     */
    std::shared_ptr<void> m_drvimpl;

    /**
     * @brief Signal: Channel cue state
     */
    SignalChannelState s_cue;

    /**
     * @brief Signal: Channel mute state
     */
    SignalUintBool s_mute;

    /**
     * @brief Signal: Channel solo state
     */
    SignalUintBool s_solo;

    /**
     * @brief Signal: Control cue state
     */
    SignalState s_control_cue;

    /**
     * @brief Signal: Tap tempo input
     */
    SignalVoid s_taptempo;

    /**
     * @brief Signal: Transport time signature
     */
    SignalUInt2 s_timesig;

    /**
     * @brief Current index of preset configured control cue states
     *
     */
    uint m_control_cue_ptr;

    /**
     * @brief Current control cue state
     */
    STATE m_control_cue;

    /**
     * @brief Preset configured time signatures
     */
    std::vector<std::pair<uint, uint>> m_timesigs;

    /**
     * @brief Currently selected index in list of configured time signatures
     */
    uint m_timesig_idx;
};
