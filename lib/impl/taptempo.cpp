/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <chrono>
#include <iostream>

#include "taptempo.h"

TapTempo::TapTempo()
    : m_tap_start(0),
      m_tap_count(0)
{
}

TapTempo::~TapTempo()
{
}

void TapTempo::on_tap()
{
    const int64_t now = std::chrono::steady_clock::now().time_since_epoch().count();
    const auto diff = (now - m_tap_start) / 1.e9;
    if (diff > 4.0)
    {
        // reset; no emit
        m_tap_start = now;
        m_tap_count = 0;
    }
    else
    {
        m_tap_count++;
        const auto tps = m_tap_count / diff;
        const auto bpm = tps * 60.0;
        std::cout << "TapTempo"
                  << " tps = " << tps
                  << " bpm = " << bpm
                  << std::endl;
        s_tempo.emit(bpm);
    }
}