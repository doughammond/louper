/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include "hardware.h"

// ----

InterruptHandler::InterruptHandler()
{
}

InterruptHandler::~InterruptHandler()
{
    stop();
}

void InterruptHandler::setup(const uint8_t pin)
{
    if (m_workers.contains(pin))
    {
        std::cout << "Already working on pin" << pin << std::endl;
        return;
    }

    m_workers[pin] = std::move(std::make_shared<InterruptWorker>());
    m_workers[pin]->interrupt().connect(s_interrupt);

    m_threads[pin] = std::move(std::thread(
        [this, &pin]()
        {
            m_workers[pin]->watchPin(pin);
        }));
}

void InterruptHandler::stop()
{
    std::cout << "stop workers" << std::endl;
    for (auto &&[i, w] : m_workers)
    {
        // std::cout << ".s." << std::endl;
        w->stop();
    }
    m_workers.clear();

    std::cout << "stop threads" << std::endl;
    for (auto &&[i, t] : m_threads)
    {
        // std::cout << ".j." << std::endl;
        t.join();
    }
    // std::cout << "clear threads" << std::endl;
    m_threads.clear();
    // std::cout << "done" << std::endl;
}

// ----

#define PIN_TAP 0   // BCM 17 / PHY 11
#define PIN_MODE 25 // BCM 19 / PHY 26
#define PIN_SYS 3   // BCM 22 / PHY 15
#define PIN_CUE0 23 // BCM 13 / PHY 33
#define PIN_CUE1 4  // BCM 23 / PHY 16
#define PIN_CUE2 5  // BCM 24 / PHY 18

Hardware::Hardware()
    : m_lastPin(255),
      m_inthandler(std::move(std::make_shared<InterruptHandler>())),
      m_longPressTimer(std::move(std::make_shared<Timer>()))
{
    wiringPiSetup();

    m_inthandler->interrupt().connect(sigc::mem_fun(*this, &Hardware::onInterrupt));

    m_longPressTimer->timeout().connect(
        [this]()
        {
            if (m_lastPin != 255)
            {
                sendPress(m_lastPin, true);
            }
        });

    const auto setupPin = [this](const uint8_t pin)
    {
        pinMode(pin, INPUT);
        pullUpDnControl(pin, PUD_DOWN);
        m_inthandler->setup(pin);
    };

    setupPin(PIN_TAP);
    setupPin(PIN_MODE);
    setupPin(PIN_SYS);
    setupPin(PIN_CUE0);
    setupPin(PIN_CUE1);
    setupPin(PIN_CUE2);
}

Hardware::~Hardware()
{
}

void Hardware::onInterrupt(const uint8_t pin, const bool active)
{
    std::cout << "onInterrupt" << pin << ":" << active << std::endl;

    if (m_longPressTimer->isActive())
    {
        m_longPressTimer->stop();
    }

    m_lastPin = active ? pin : 255;

    if (active)
    {
        sendPress(pin, false);
        m_longPressTimer->start(5000);
    }
}

void Hardware::sendPress(const uint8_t pin, const bool longPress)
{
    std::cout << "sendPress" << pin << ":" << longPress << std::endl;
    switch (pin)
    {
    case PIN_TAP:
        s_tap.emit(longPress);
        break;
    case PIN_MODE:
        s_mode.emit(longPress);
        break;
    case PIN_SYS:
        s_sys.emit(longPress);
        break;
    case PIN_CUE0:
        s_cue.emit(0, longPress);
        break;
    case PIN_CUE1:
        s_cue.emit(1, longPress);
        break;
    case PIN_CUE2:
        s_cue.emit(2, longPress);
        break;
    default:
        std::cout << "unhandled pin interrupt" << pin << std::endl;
    }
}
