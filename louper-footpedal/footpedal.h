/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <memory>

#include "hardware.h"

/**
 * @brief Foot pedal remote control controller
 */
class FootPedal
{
public:
    /**
     * @brief Shared pointer type for FootPedal
     */
    using sptr = std::shared_ptr<FootPedal>;

    explicit FootPedal();
    ~FootPedal();

    /**
     * @brief Control: Start
     *
     */
    void start();

private:
    /**
     * @brief Hardware interface instance
     */
    Hardware::sptr m_hardware;

    /**
     * @brief Remote control client implementation
     */
    std::shared_ptr<void> m_clientimpl;
};
