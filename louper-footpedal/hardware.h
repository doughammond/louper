/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#pragma once

#include <iostream>
#include <map>
#include <memory>
#include <thread>
#include <sigc++/sigc++.h>

#include <wiringPi.h>

// ----

using SignalTimeout = sigc::signal<void(void)>;

/**
 * @brief A Timer. Not really implemented yet.
 */
class Timer
{
public:
    /**
     * @brief Shared pointer type for Timer
     *
     */
    using sptr = std::shared_ptr<Timer>;

    /**
     * @brief Is this timer active?
     *
     * @return true
     * @return false
     */
    bool isActive() const
    {
        return false;
    }

    /**
     * @brief Start a timer for given count of milliseconds
     *
     * @param millis
     */
    void start(const uint millis)
    {
        // TODO - but only if we need long press
    }

    /**
     * @brief Stop the timer
     *
     */
    void stop()
    {
        // TODO - but only if we need long press
    }

    /**
     * @brief Getter for timeout signal
     *
     * @return SignalTimeout&
     */
    SignalTimeout &timeout()
    {
        return s_timeout;
    }

private:
    /**
     * @brief Signal: timer period elapsed
     *
     */
    SignalTimeout s_timeout;
};

// ----

// uint8_t pin, bool active
using SignalInterrupt = sigc::signal<void(const uint8_t, const bool)>;

/**
 * @brief Worker for an individual pin interrupt monitor
 */
class InterruptWorker
{
public:
    /**
     * @brief Shared pointer type for InterruptWorker
     *
     */
    using sptr = std::shared_ptr<InterruptWorker>;

    /**
     * @brief Worker control: Start
     *
     * @param pin
     */
    void watchPin(const uint8_t pin)
    {
        int state = 0;
        //        int iter = 0;
        while (m_running)
        {
            int i = digitalRead(pin);
            if (i == HIGH && state == 0)
            {
                state = 1;
                std::cout << "watch pin" << pin << "got change ACTIVE" << std::endl;
                s_interrupt.emit(pin, true);
                // std::cout << "emit interrupt finished" << std::endl;
            }
            else if (i == LOW && state == 1)
            {
                state = 0;
                std::cout << "watch pin" << pin << "got change INACTIVE" << std::endl;
                s_interrupt.emit(pin, false);
                // std::cout << "emit interrupt finished" << std::endl;
            }
            delay(1);
            //            if (++iter == 1000) {
            //                // std::cout << "interrupt poll alive : " << i << " / " << state << std::endl;
            //                iter = 0;
            //                if (i == 1 && state == 1) {
            //                    // std::cout << "reset int detect state" << std::endl;
            //                    state = 0;
            //                }
            //            }
        }
        std::cout << "worker loop exit" << std::endl;
    }

    /**
     * @brief Worker control: Stop
     */
    void stop()
    {
        std::cout << "worker set end" << std::endl;
        m_running = false;
    }

    /**
     * @brief Getter for input interrupt signal
     *
     * @return SignalInterrupt&
     */
    SignalInterrupt &interrupt()
    {
        return s_interrupt;
    }

private:
    /**
     * @brief Signal: input interrupt
     */
    SignalInterrupt s_interrupt;

private:
    /**
     * @brief Is this worker running?
     */
    bool m_running = true;
};

// ----

/**
 * @brief Controller for all pin interrupts
 */
class InterruptHandler
{
public:
    /**
     * @brief Shared pointer type for InterruptHandler
     *
     */
    using sptr = std::shared_ptr<InterruptHandler>;

    explicit InterruptHandler();
    ~InterruptHandler();

    /**
     * @brief Handler control: Start
     *
     * @param pin
     */
    void setup(const uint8_t pin);

    /**
     * @brief Handler control: Stop
     *
     */
    void stop();

    /**
     * @brief Getter for input interrupt signal
     *
     * @return SignalInterrupt&
     */
    SignalInterrupt &interrupt()
    {
        return s_interrupt;
    }

private:
    /**
     * @brief Signal for input interrupt
     *
     */
    SignalInterrupt s_interrupt;

private:
    /**
     * @brief Worker instances
     */
    std::map<uint8_t, InterruptWorker::sptr> m_workers;

    /**
     * @brief Worker threads
     */
    std::map<uint8_t, std::thread> m_threads;
};

// ----

// bool longPress
using SignalCommand = sigc::signal<void(bool)>;
// uint8_t idx, bool longPress
using SignalCue = sigc::signal<void(uint8_t, bool)>;

/**
 * @brief Hardware interface controller
 */
class Hardware
{
public:
    /**
     * @brief Shared pointer type for Hardware
     *
     */
    using sptr = std::shared_ptr<Hardware>;

    explicit Hardware();
    ~Hardware();

public:
    /**
     * @brief Slot: Handler for pin interrupt
     */
    void onInterrupt(const uint8_t pin, const bool active);

    /**
     * @brief Dispatch signal appropriate for activated pin number
     */
    void sendPress(const uint8_t pin, const bool active);

    /**
     * @brief Getter for tap input signal
     */
    SignalCommand &tap()
    {
        return s_tap;
    }

    /**
     * @brief Getter for mode input signal
     */
    SignalCommand &mode()
    {
        return s_mode;
    }

    /**
     * @brief Getter for system input signal
     */
    SignalCommand &sys()
    {
        return s_sys;
    }

    /**
     * @brief Getter for cue input signal
     */
    SignalCue &cue()
    {
        return s_cue;
    }

private:
    /**
     * @brief Signal for tap input
     */
    SignalCommand s_tap;

    /**
     * @brief Signal for mode input
     */
    SignalCommand s_mode;

    /**
     * @brief Signal for system input
     */
    SignalCommand s_sys;

    /**
     * @brief Signal for cue input
     */
    SignalCue s_cue;

private:
    /**
     * @brief Last pressed pin number
     */
    uint8_t m_lastPin;

    /**
     * @brief Input interrupt listener/handler
     */
    InterruptHandler::sptr m_inthandler;

    /**
     * @brief Timer for detecting button long-press
     */
    Timer::sptr m_longPressTimer;
};
