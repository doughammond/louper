/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>

#include "config.h"
#include "footpedal.h"

int main(int argc, char *argv[])
{
    std::cerr << LOOPER_NAME
              << " FootPedal version " << LOOPER_VER << " " << LOOPER_VER_DATE
              << std::endl
              << LOOPER_LICENSE
              << std::endl
              << LOOPER_COPYRIGHT
              << std::endl
              << std::endl;

    const auto pedal = std::make_shared<FootPedal>();
    pedal->start();

    return 0;
}
