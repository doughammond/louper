/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2022 Doug Hammond */

#include <iostream>
#include <lo/lo.h>
#include <lo/lo_cpp.h>

#include "footpedal.h"

FootPedal::FootPedal()
    : m_hardware(std::move(std::make_shared<Hardware>()))
{
    std::shared_ptr<lo::Address> a = std::make_shared<lo::Address>("localhost", 9000);

    m_hardware->tap().connect(
        [a](const bool longPress)
        {
            if (!longPress)
            {
                std::cout << "FootPedal send taptempo" << std::endl;
                a->send("taptempo");
            }
            else
            {
                std::cout << "FootPedal send timesig" << std::endl;
                a->send("timesig");
            }
        });

    m_hardware->mode().connect(
        [a](const bool longPress)
        {
            if (!longPress)
            {
                std::cout << "FootPedal send mode" << std::endl;
                a->send("mode");
            }
        });

    m_hardware->sys().connect(
        [a](const bool longPress)
        {
            if (!longPress)
            {
                std::cout << "FootPedal send clearall" << std::endl;
                a->send("clearall");
            }
        });

    m_hardware->cue().connect(
        [a](const uint8_t idx, const bool longPress)
        {
            if (!longPress)
            {
                std::cout << "FootPedal send cue=" << idx << std::endl;
                a->send("cue", "i", idx);
            }
        });

    m_clientimpl = std::static_pointer_cast<void>(a);
}

FootPedal::~FootPedal()
{
}

void FootPedal::start()
{
    while (true)
    {
        delay(1);
    }
}
