/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <iostream>
#include <thread>

#include <argparse/argparse.hpp>

#ifdef USE_JET_LIVE
#include <jet/live/Live.hpp>
#include <jet/live/Utility.hpp>
#endif

#include "config.h"
#include "looper.h"

#include "contexts/application.h"
#include "contexts/imgui.h"
#include "contexts/sdl.h"

int main(int argc, char *argv[])
{
  std::cerr << LOOPER_NAME
            << " GUI version " << LOOPER_VER << " " << LOOPER_VER_DATE
            << std::endl
            << LOOPER_LICENSE
            << std::endl
            << LOOPER_COPYRIGHT
            << std::endl
            << std::endl;

  argparse::ArgumentParser program(std::string(LOOPER_NAME) + "-gui");

  program.add_argument("-r", "--sample-rate")
      .default_value(44100)
      .required()
      .action([](const std::string &value)
              { return std::stoi(value); })
      .help("synthesis sample rate");

  program.add_argument("-b", "--buffer-size")
      .default_value(256)
      .required()
      .action([](const std::string &value)
              { return std::stoi(value); })
      .help("audio buffer size");

  program.add_argument("-s", "--scale")
      .default_value(1.0f)
      .required()
      .action([](const std::string &value)
              { return std::stof(value); })
      .help("GUI scale");

  try
  {
    program.parse_args(argc, argv);
  }
  catch (const std::runtime_error &err)
  {
    std::cout << err.what() << std::endl;
    std::cout << program;
    exit(-1);
  }

  const auto Fs = program.get<int>("-r");
  const auto bufsize = program.get<int>("-b");
  const auto scale = program.get<float>("-s");

  using namespace looper::gui;

  auto sdl = std::make_shared<contexts::SDLContext>("Looper");
  if (sdl->state() != 0)
  {
    std::cerr << "Did not initialise SDL" << std::endl;
    return -1;
  }

  auto imgui = std::make_shared<contexts::ImGuiContext>(sdl, scale);
  if (imgui->state() != 0)
  {
    std::cerr << "Did not initialise Dear Imgui" << std::endl;
    return -1;
  }

  const uint CHANNEL_COUNT = 3;
  auto app = std::make_shared<contexts::Application>(sdl, imgui, CHANNEL_COUNT);
  auto looper = std::make_shared<Looper>(CHANNEL_COUNT, Fs, bufsize);

  looper->transport_state().connect(sigc::mem_fun(*app, &contexts::Application::on_transport_state));
  looper->tempo().connect(sigc::mem_fun(*app, &contexts::Application::on_tempo));
  looper->timesig().connect(sigc::mem_fun(*app, &contexts::Application::on_timesig));
  looper->bar_length().connect(sigc::mem_fun(*app, &contexts::Application::on_bar_length));

  looper->channel_cue().connect(sigc::mem_fun(*app, &contexts::Application::on_channel_cue));
  looper->channel_state().connect(sigc::mem_fun(*app, &contexts::Application::on_channel_state));
  looper->channel_length().connect(sigc::mem_fun(*app, &contexts::Application::on_channel_length));
  looper->channel_position().connect(sigc::mem_fun(*app, &contexts::Application::on_channel_position));
  looper->levels().connect(sigc::mem_fun(*app, &contexts::Application::on_levels));
  looper->control_cue().connect(sigc::mem_fun(*app, &contexts::Application::on_control_cue));

#ifdef USE_JET_LIVE
  struct ExampleListener : public jet::ILiveListener
  {
    void onLog(jet::LogSeverity severity, const std::string &message) override
    {
      std::cout << "Live " << (int)severity << ": " << message << std::endl;
    }
    void onCodePreLoad() override
    {
      std::cout << "onCodePreload" << std::endl;
    }
    void onCodePostLoad() override
    {
      std::cout << "onCodePostload" << std::endl;
    }
  };
  auto listener = jet::make_unique<ExampleListener>();
  jet::LiveConfig config;
  config.workerThreadsCount = 6;
  auto *jl = new jet::Live(std::move(listener), config);
  while (!jl->isInitialized())
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    jl->update();
  }
  sdl->registerKeyCommand(
      {
          .sym = SDLK_r,
          .mod = KMOD_CTRL,
      },
      [jl]()
      {
        std::cout << "Start jet live reload" << std::endl;
        jl->tryReload();
      });
#endif

  std::thread loopthread(
      [looper]()
      {
        looper->start();
      });

  const auto quitAction = [looper, &loopthread, app]()
  {
    looper->stop();
    app->quit();
  };
  sdl->registerNamedAction("quit", quitAction);
  sdl->registerKeyCommand(
      {
          .sym = SDLK_q,
          .mod = KMOD_CTRL,
      },
      quitAction);

#ifdef USE_JET_LIVE
  imgui->renderLoop([sdl, imgui, app, jl]()
                    {
    jl->update();
#else
  imgui->renderLoop([sdl, imgui, app]()
                    {
#endif
    sdl->onFrame();
    imgui->onFrame();
    app->onFrame(); });

  loopthread.join();

  imgui = nullptr;
  sdl = nullptr;

  return 0;
}
