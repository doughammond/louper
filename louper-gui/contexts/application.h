/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <imgui.h>
#include <imgui_stdlib.h>

#include "imgui.h"
#include "sdl.h"

#include "looper.h"

/**
 * @brief Namespace for all GUI rendering contexts.
 *
 */
namespace looper::gui::contexts
{
  /**
   * @brief Callback function for a Prompt choice.
   *
   */
  using PromptCallback = std::function<bool()>;

  /**
   * @brief User-prompt UI.
   *
   */
  struct Prompt
  {
    /**
     * @brief Text to display to the user for this prompt.
     *
     */
    std::string text;

    /**
     * @brief Map of user-choices for this prompt.
     *
     */
    std::unordered_map<std::string, PromptCallback> choices;
  };

  /**
   * @brief Application GUI renderer implementation.
   *
   */
  class Application
  {
  public:
    /**
     * @brief Shared pointer type for this context.
     *
     */
    using shared_ptr = std::shared_ptr<Application>;

    /**
     * @brief Construct a new Application object.
     *
     * @param sdl
     * @param imgui
     * @param channels number of loop recorder channels
     */
    Application(
        SDLContext::shared_ptr sdl,
        ImGuiContext::shared_ptr imgui,
        const uint &channels);

    /**
     * @brief Destroy the Application object.
     *
     */
    ~Application();

    /**
     * @brief Getter for the context initialisation state.
     *
     * @return const int
     */
    const int state() const
    {
      return m_state;
    }

    /**
     * @brief Context onFrame handler; called for every rendering frame.
     *
     */
    void onFrame();

    /**
     * @brief Quit the application.
     *
     */
    void quit();

    /**
     * @brief Slot: Transport clock changed
     *
     * @param bar
     * @param beat
     */
    void on_transport_state(const uint &bar, const uint &beat)
    {
      m_bar = bar;
      m_beat = beat;
    }

    /**
     * @brief Slot: Transport tempo changed
     *
     * @param bpm
     */
    void on_tempo(const float &bpm)
    {
      m_bpm = bpm;
    }

    /**
     * @brief Slot: time signature changed
     *
     * @param num
     * @param den
     */
    void on_timesig(const uint &num, const uint &den)
    {
      m_ts_num = num;
      m_ts_den = den;
    }

    /**
     * @brief Slot: Bar buffer length changed
     *
     * @param len
     */
    void on_bar_length(const uint32_t &len)
    {
      m_bar_length = len;
    }

    /**
     * @brief Slot: Loop recorder channel cue state changed
     *
     * @param channel
     * @param state
     */
    void on_channel_cue(const uint &channel, const STATE &state)
    {
      m_channel_cue[channel] = state;
    }

    /**
     * @brief Slot: Loop recorder channel state changed
     *
     * @param channel
     * @param state
     */
    void on_channel_state(const uint &channel, const STATE &state)
    {
      m_channel_state[channel] = state;
    }

    /**
     * @brief Slot: Loop recorder channel buffer length changed
     *
     * @param channel
     * @param length
     */
    void on_channel_length(const uint &channel, const uint &length)
    {
      m_channel_length[channel] = length;
    }

    /**
     * @brief Slot: Loop recorder channel buffer position changed
     *
     * @param channel
     * @param position
     */
    void on_channel_position(const uint &channel, const uint &position)
    {
      m_channel_position[channel] = position;
    }

    /**
     * @brief Slot: Channel metering levels changed
     *
     * @param channel
     * @param levels
     * @param mute
     * @param solo
     */
    void on_levels(const int &channel, const MeterLevels &levels, const bool &mute, const bool &solo)
    {
      m_levels[channel] = levels;
      if (mute)
      {
        m_channel_mute[channel] = mute;
      }
      else
      {
        m_channel_mute.erase(channel);
      }
      if (solo)
      {
        m_channel_solo[channel] = solo;
      }
      else
      {
        m_channel_solo.erase(channel);
      }
    }

    /**
     * @brief Slot: control cue state changed
     *
     * @param state
     */
    void on_control_cue(const STATE &state)
    {
      m_control_cue = state;
    }

    // GUI state
  private:
    /**
     * @brief Render the Transport component
     */
    void render_Transport();

    /**
     * @brief Render a Loop recorder channel component
     */
    void render_Channel(const uint &idx);

    /**
     * @brief Render the control cue component
     */
    void render_Cue();

    /**
     * @brief Render the status panel.
     *
     */
    void render_Status(const uint &top, const uint &width, const uint &height);

    /**
     * @brief Conditional: Render a user prompt; if m_prompt contains choices.
     *
     */
    void render_Prompt();

    // Internal state

    /**
     * @brief This context's initialisation state.
     *
     */
    int m_state;

    /**
     * @brief The SDL context.
     *
     */
    SDLContext::shared_ptr m_sdl;

    /**
     * @brief The Dear ImGui context.
     *
     */
    ImGuiContext::shared_ptr m_imgui;

    /**
     * @brief DPI scaling factor; cached value from m_imgui
     */
    float m_dpiScale;

    /**
     * @brief User prompt information.
     *
     */
    Prompt m_prompt;

    // Looper state
  private:
    /**
     * @brief Number of loop recorder channels
     */
    uint m_channels;

    /**
     * @brief Current time: bar
     */
    uint m_bar;

    /**
     * @brief Current time: beat
     */
    uint m_beat;

    /**
     * @brief Current tempo
     */
    float m_bpm;

    /**
     * @brief Current time signature numerator
     */
    uint m_ts_num;

    /**
     * @brief Current time signature denominator
     */
    uint m_ts_den;

    /**
     * @brief Current bar buffer length
     */
    uint32_t m_bar_length;

    /**
     * @brief Current loop recorder channel cue states
     */
    std::unordered_map<uint, STATE> m_channel_cue;

    /**
     * @brief Current loop recorder channel states
     */
    std::unordered_map<uint, STATE> m_channel_state;

    /**
     * @brief Current loop recorder channel buffer lengths
     */
    std::unordered_map<uint, uint> m_channel_length;

    /**
     * @brief Current loop recorder channel buffer positions
     */
    std::unordered_map<uint, uint> m_channel_position;

    /**
     * @brief Current channel metering levels
     */
    std::unordered_map<int, MeterLevels> m_levels;

    /**
     * @brief Current channel mute states
     */
    std::unordered_map<int, bool> m_channel_mute;

    /**
     * @brief Current channel solo states
     */
    std::unordered_map<int, bool> m_channel_solo;

    /**
     * @brief Current control cue command
     */
    STATE m_control_cue;
  };

} // namespace looper::gui::contexts
