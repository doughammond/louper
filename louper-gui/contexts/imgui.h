/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <functional>
#include <memory>

#include <SDL2/SDL.h>
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl2.h>

#include "../gl-setup.h"

#include "sdl.h"

namespace looper::gui::contexts
{
  /**
   * @brief GUI Dear ImGui rendering context.
   *
   */
  class ImGuiContext
  {
  public:
    /**
     * @brief Shared pointer type for this context.
     *
     */
    using shared_ptr = std::shared_ptr<ImGuiContext>;

    /**
     * @brief Type for the application render function, called in a loop.
     *
     */
    using RenderLoopFn = std::function<void()>;

    /**
     * @brief Construct a new Im Gui Context object.
     *
     * @param sdl
     * @param dpiScale
     */
    ImGuiContext(SDLContext::shared_ptr sdl, const float dpiScale = 1.f);

    /**
     * @brief Destroy the Im Gui Context object.
     *
     */
    ~ImGuiContext();

    /**
     * @brief Get the context initialisation state.
     *
     * @return const int
     */
    const int state() const
    {
      return m_state;
    }

    /**
     * @brief Get the DPI scaling value.
     *
     * @return const float
     */
    const float dpiScale() const
    {
      return m_dpiScale;
    }

    /**
     * @brief Get a pointer to the SDL context.
     *
     * @return SDLContext::shared_ptr
     */
    SDLContext::shared_ptr sdl() const
    {
      return m_sdl;
    }

    /**
     * @brief Tell the application loop that we are done and want to exit.
     *
     */
    void quit()
    {
      std::cout << "setting m_done" << std::endl;
      m_done = true;
    }

    /**
     * @brief Context onFrame handler; called for every rendering frame.
     *
     * @return SDLKeyEvents
     */
    void onFrame();

    /**
     * @brief Start the main application render loop.
     *
     * @param fn
     */
    void renderLoop(RenderLoopFn fn);

    /**
     * @brief Get the current UI rendering frame rate.
     *
     * @return const float
     */
    const float frameRate() const
    {
      return m_io->Framerate;
    }

    /**
     * @brief Get pointer to normal (1x) font
     *
     * @return ImFont*
     */
    ImFont *font_normal() const
    {
      return m_font_normal;
    }

    /**
     * @brief Get pointer to large (2x) font
     *
     * @return ImFont*
     */
    ImFont *font_large() const
    {
      return m_font_large;
    }

  private:
    /**
     * @brief Set up the IO configuration.
     *
     * @param dpiScale
     */
    void setupIO(const float dpiScale);

    /**
     * @brief Set up the application fonts.
     *
     * @param dpiScale
     */
    void setupFonts(const float dpiScale);

    /**
     * @brief Set up the application style.
     *
     * @param dpiScale
     */
    void setupStyle(const float dpiScale);

    /**
     * @brief The initialisation state of this context.
     *
     */
    int m_state;

    /**
     * @brief DPI scaling support scale factor.
     *
     */
    float m_dpiScale;

    /**
     * @brief Pointer to the SDL context.
     *
     */
    SDLContext::shared_ptr m_sdl;

    /**
     * @brief Pointer to the ImGui IO configuration.
     *
     */
    ImGuiIO *m_io;

    /**
     * @brief Pointer to normal (1x) font
     *
     */
    ImFont *m_font_normal;

    /**
     * @brief Pointer to large (2x) font
     *
     */
    ImFont *m_font_large;

    /**
     * @brief Whether we should be done rendering; i.e. !(quit).
     *
     */
    bool m_done;
  };

} // namespace looper::gui::contexts
