/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <chrono>
#include <iostream>

#include <memory>
#include <string>
#include <stdexcept>

template <typename... Args>
std::string string_format(const std::string &format, Args... args)
{
  int size_s = std::snprintf(nullptr, 0, format.c_str(), args...) + 1; // Extra space for '\0'
  if (size_s <= 0)
  {
    throw std::runtime_error("Error during formatting.");
  }
  auto size = static_cast<size_t>(size_s);
  auto buf = std::make_unique<char[]>(size);
  std::snprintf(buf.get(), size, format.c_str(), args...);
  return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}

#include "application.h"

namespace colours
{
  namespace vec
  {
    const ImVec4 white(1.0, 1.0, 1.0, 1.0);
    const ImVec4 red(0.94f, 0.0f, 0.33f, 1.0f); // rgba(240, 0, 84, 1)
    const ImVec4 green(0.1f, 0.8f, 0.1f, 1.0f);
    const ImVec4 grey(0.5f, 0.5f, 0.5f, 1.0f);
  }
  namespace u32
  {
    // ImU32 as colour is in 0xAABBGGRR order
    const ImU32 white = 0xFFFFFFFF;
    const ImU32 red = 0xFF5400F0;     // rgba(240, 0, 84, 1)
    const ImU32 darkred = 0xFF2100BD; // rgba(189, 0, 33, 1)
    const ImU32 green = 0xFF11CC11;
    const ImU32 transgreen = 0x6611CC11;
    const ImU32 grey = 0xFF888888;
  }
} // namespace colours

namespace looper::gui::contexts
{
  /**
   * @brief Add vertical padding
   *
   * @param p
   */
  void PadY(const uint &p)
  {
    ImGui::SetCursorPosY(ImGui::GetCursorPosY() + p);
  }

  /**
   * @brief Render centered text
   *
   * @param text
   */
  void TextCentered(const std::string &text)
  {
    auto windowWidth = ImGui::GetWindowSize().x;
    auto textWidth = ImGui::CalcTextSize(text.c_str()).x;

    ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
    ImGui::Text(text.c_str());
  }

  /**
   * @brief Render blinking text
   *
   * @param str
   */
  void TextBlink(const std::string &str)
  {
    const auto ts = std::chrono::system_clock::now().time_since_epoch().count();
    const uint tp = (uint)floor(ts / 0.1e9) % 2; // tenth-second clock
    if (tp)
    {
      ImGui::PushStyleColor(ImGuiCol_Text, colours::vec::red);
      TextCentered(str);
      ImGui::PopStyleColor();
    }
    else
    {
      TextCentered(str);
    }
  }

  /**
   * @brief Render a VU level meter component
   *
   * @param dpiScale
   * @param pos
   * @param size
   * @param levels
   */
  void VUMeter(const float &dpiScale, const ImVec2 &pos, const ImVec2 &size, const MeterLevels &levels)
  {
    // VU bar position and dimensions
    ImDrawList *drawList = ImGui::GetWindowDrawList();
    const auto mw = (size.x - 2 * dpiScale);

    // RMS meter
    const auto rl = mw - mw * levels.RMSdB / Meter::DB_MIN;
    drawList->AddRectFilled(ImVec2(pos.x, pos.y), ImVec2(pos.x + rl, pos.y + size.y), colours::u32::darkred, 2.0 * dpiScale);

    // Peak bar
    const auto pl = -2 * dpiScale + mw - mw * levels.peakdB / Meter::DB_MIN;
    drawList->AddLine(ImVec2(pos.x + pl, pos.y), ImVec2(pos.x + pl, pos.y + size.y), colours::u32::red, 2.0f);
  }

  Application::Application(
      SDLContext::shared_ptr sdl,
      ImGuiContext::shared_ptr imgui,
      const uint &channels)
      : m_state(-2),
        m_sdl(sdl),
        m_imgui(imgui),
        m_dpiScale(imgui->dpiScale()),
        m_channels(channels)
  {
    for (uint i = 0; i < channels; ++i)
    {
      m_channel_cue[i] = STATE::CLEAR;
      m_channel_state[i] = STATE::CLEAR;
      m_channel_length[i] = 0;
      m_channel_position[i] = 0;
    }
    m_control_cue = ControlStates[0];

    m_state = 0;
    std::cerr << "Application context created "
              << std::endl;
  }

  Application::~Application()
  {
    std::cerr << "Application context destroyed" << std::endl;
  }

  /**
   * @brief Flags for creating a fixed position, fixed size window with no title.
   *
   */
  const ImGuiWindowFlags immovableWindow = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;

  void Application::onFrame()
  {
    const auto ws = m_sdl->windowSize();

    // const int statusHeight = 32 * m_dpiScale;
    const int workspaceHeight = 480 * m_dpiScale; // std::max(ws[1] - statusHeight, 0);

    const int editorWidth = 800 * m_dpiScale; // std::max(ws[0], 0);
    ImVec2 looperGuiPos(0, 0);
    ImGui::SetNextWindowPos(looperGuiPos);
    ImVec2 looperGuiSize(editorWidth, workspaceHeight);
    ImGui::SetNextWindowSize(looperGuiSize);

    ImGui::Begin("Looper", NULL, immovableWindow);

    // Draw the Looper GUI in here
    render_Transport();

    for (uint i = 0; i < m_channels; ++i)
    {
      render_Channel(i);
      if (i != m_channels - 1)
      {
        ImGui::SameLine();
      }
    }

    render_Cue();

    ImGui::End();

    // render_Status(ws[1] - statusHeight, ws[0], statusHeight);
    // render_Prompt();
  }

  void Application::quit()
  {
    m_imgui->quit();
  }

  void Application::render_Transport()
  {
    ImGui::BeginChildFrame(2000, ImVec2(250 * m_dpiScale, 50 * m_dpiScale));
    PadY(2 * m_dpiScale);
    ImGui::PushFont(m_imgui->font_large());
    TextCentered(string_format("%0.2f bpm", m_bpm));
    ImGui::PopFont();
    ImGui::EndChildFrame();

    ImGui::SameLine();

    ImGui::BeginChildFrame(2001, ImVec2(250 * m_dpiScale, 50 * m_dpiScale));
    // TODO: put the time sig in this box?
    ImDrawList *drawList = ImGui::GetWindowDrawList();
    const auto rad = 8 * m_dpiScale;
    const auto marg = 3 * rad;
    if (m_ts_den > 0)
    {

      for (uint b = 0; b < 4 * m_ts_num / m_ts_den; ++b)
      {
        const auto p1 = ImGui::GetItemRectMin();
        const auto p2 = ImGui::GetItemRectMax();

        if (m_beat == b + 1)
        {
          drawList->AddCircleFilled(ImVec2(p1.x + 2 * marg + (b * 6 * rad + rad), p2.y + marg), rad, colours::u32::green);
        }
        {
          drawList->AddCircle(ImVec2(p1.x + 2 * marg + (b * 6 * rad + rad), p2.y + marg), rad, colours::u32::grey);
        }

        ImGui::SameLine();
      }
    }
    ImGui::EndChildFrame();

    ImGui::SameLine();

    ImGui::BeginChildFrame(2002, ImVec2(250 * m_dpiScale, 50 * m_dpiScale));
    {
      // Input monitor
      if (m_levels.find(m_channels) != m_levels.end())
      {
        const auto p1 = ImGui::GetItemRectMin();
        const ImVec2 pos{p1.x + 2 * m_dpiScale, 35 * m_dpiScale};
        const ImVec2 size{250 * m_dpiScale, 10 * m_dpiScale};
        VUMeter(m_dpiScale, pos, size, m_levels[m_channels]);
      }

      // Output
      if (m_levels.find(-1) != m_levels.end())
      {
        const auto p1 = ImGui::GetItemRectMin();
        const ImVec2 pos{p1.x + 2 * m_dpiScale, 50 * m_dpiScale};
        const ImVec2 size{250 * m_dpiScale, 10 * m_dpiScale};
        VUMeter(m_dpiScale, pos, size, m_levels[-1]);
      }
    }
    ImGui::EndChildFrame();
  }

  void Application::render_Channel(const uint &idx)
  {
    const auto w = 250 * m_dpiScale;
    const auto h = 220 * m_dpiScale;

    ImGui::BeginChildFrame(1000 + idx, ImVec2(w, h));

    const auto p1 = ImGui::GetItemRectMin();
    const auto left = p1.x + 2 * m_dpiScale;

    PadY(5 * m_dpiScale);
    ImGui::PushFont(m_imgui->font_large());
    TextCentered(string_format("%d            ", 1 + idx));
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Text, colours::vec::red);
    TextCentered(string_format("         %s   ", m_channel_mute.find(idx) != m_channel_mute.end() ? "M" : " "));
    ImGui::PopStyleColor();
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Text, colours::vec::green);
    TextCentered(string_format("            %s", m_channel_solo.find(idx) != m_channel_solo.end() ? "S" : " "));
    ImGui::PopStyleColor();
    ImGui::PopFont();

    PadY(5 * m_dpiScale);

    const auto cue = m_channel_cue[idx];
    const auto state = m_channel_state[idx];

    ImGui::PushFont(m_imgui->font_large());
    if (cue == state)
    {
      if (state != STATE::CLEAR)
      {
        TextCentered(StateName(state));
      }
    }
    else
    {
      TextBlink(StateName(cue));
    }
    ImGui::PopFont();

    if (state != STATE::CLEAR)
    {
      if (m_channel_length[idx] > 0 && m_bar_length > 0)
      {
        TextCentered(string_format("%0.f bars", ceil(m_channel_length[idx] / m_bar_length)));
      }

      const auto p2 = ImGui::GetItemRectMax();
      const auto top = 15 * m_dpiScale + p2.y;

      if (m_channel_position[idx] > 0 && m_channel_length[idx] > 0)
      {
        const auto progress = (float)m_channel_position[idx] / m_channel_length[idx];
        if (progress > 0)
        {
          const auto h = 15 * m_dpiScale;
          ImDrawList *drawList = ImGui::GetWindowDrawList();
          drawList->AddRectFilled(ImVec2(left, top), ImVec2(left + (w - 4 * m_dpiScale), top + h), colours::u32::transgreen, 2.0 * m_dpiScale);
          drawList->AddCircleFilled(ImVec2(left + (w - 4 * m_dpiScale) * progress, top + h / 2.f), h / 2.f, colours::u32::green);
        }
      }

      if (m_levels.find(idx) != m_levels.end())
      {
        const ImVec2 pos{left, 20 * m_dpiScale + top};
        const ImVec2 size{w, 10 * m_dpiScale};
        VUMeter(m_dpiScale, pos, size, m_levels[idx]);
      }
    }

    ImGui::EndChildFrame();
  }

  void Application::render_Cue()
  {
    const auto w = (750 + 24) * m_dpiScale;
    const auto h = 40 * m_dpiScale;

    ImGui::BeginChildFrame(3000, ImVec2(w, h));
    ImGui::PushFont(m_imgui->font_large());
    ImGui::PushStyleColor(ImGuiCol_Text, colours::vec::red);
    TextCentered(StateName(m_control_cue));
    ImGui::PopStyleColor();
    ImGui::PopFont();
    ImGui::EndChildFrame();
  }

  void Application::render_Status(const uint &top, const uint &width, const uint &height)
  {
    ImVec2 statusPos(0, top);
    ImGui::SetNextWindowPos(statusPos);
    ImVec2 statusSize(width, height);
    ImGui::SetNextWindowSize(statusSize);

    ImGui::Begin("Status", NULL, immovableWindow);
    {
      ImGui::Text(
          "UI FPS: %0.2f",
          m_imgui->frameRate());
    }
    ImGui::End();
  }

  void Application::render_Prompt()
  {
    bool haveChoices = m_prompt.choices.size() > 0;
    if (haveChoices)
    {
      ImGui::OpenPopup("Question");
    }
    if (ImGui::BeginPopupModal("Question", &haveChoices))
    {
      bool didAction = false;
      ImGui::TextUnformatted(m_prompt.text.c_str());
      ImGui::Separator();
      ImGui::BeginGroup();
      {
        for (const auto &[label, action] : m_prompt.choices)
        {
          ImGui::SameLine();
          if (ImGui::Button(label.c_str()))
          {
            didAction = action();
          }
        }
      }
      ImGui::EndGroup();

      if (didAction)
      {
        m_prompt.text.clear();
        m_prompt.choices.clear();
        haveChoices = false;
      }

      ImGui::EndPopup();
    }
  }

} // namespace lopper::gui::contexts
